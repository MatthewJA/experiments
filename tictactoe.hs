import Data.List.Split (splitOn)
import Data.List (maximumBy)

import TicTacToeBoard

parse_move :: String -> (Integer, Integer)
parse_move string = do
	(move_list !! 0, move_list !! 1)
	where
		move_list = map read
			. splitOn ","
			. filter ((/=) ' ')
			$ string

make_move :: Board -> (Integer, Integer) -> Symbol -> Maybe Board
make_move board coordinates symbol
	| get_symbol board coordinates /= Blank = Nothing
	| otherwise = Just $ set_symbol board coordinates symbol

get_game_finisher :: Board -> Symbol -> Maybe (Integer, Integer)
get_game_finisher board symbol
	| (length . filter (Blank==) $ flatten_rows board) == 1 = -- If there is exactly one blank
		Just
			. head -- Get the first element of
			. filter ((Blank==) . get_symbol board) -- The list of coordinates of blanks
			$ all_coordinates -- On the board.
	| length finishers > 0 = head finishers
	| otherwise = Nothing
	where
		all_coordinates = [(x, y) | x <- [0..2], y <- [0..2]]
		all_coordinate_pairs = [(i, j) | i <- all_coordinates, j <- all_coordinates]
		finishers = filter handler all_coordinate_pairs
			where
				handler ((a, b) (c, d)) =
					-- Check whether the remaining square in the row/column is blank, and whether the symbols
					-- at the coordinates are equal. True if fulfilling these conditions, false otherwise.
					(get_symbol board (a, b)) == (get_symbol board (c, d)) && (get_symbol board remaining_square) == Blank
					where
						remaining_square
							| a == c = (a, head . filter (\x -> x \= b && x \= d) $ [0..2]) -- Column.
							| b == d = (b, head . filter (\x -> x \= a && x \= c) $ [0..2]) -- Row.
							| a == b && c == d = ((head . filter (\x -> x \= a && x \= c) $ [0..2]),
												  (head . filter (\x -> x \= a && x \= c) $ [0..2])) -- Diagonal.
							| a == d && c == b = ((head . filter (\x -> x \= a) $ [0..2]),
												  (head . filter (\x -> x \= c) $ [0..2])) -- Diagonal.
							| otherwise = error "Oddities found in already bizarre code."

board_quality :: Board -> Symbol -> Integer
board_quality board symbol =

best_move :: Board -> Symbol -> (Integer, Integer)
best_move board symbol = case game_finisher of
	Just coordinates -> coordinates
	Nothing ->
		maximumBy compare_qualities
		. filter (Nothing /=)
		. map (\x -> make_move board x O)
		$ [(x, y) | x <- [0..2], y <- [0..2]]
	where
		game_finisher = get_game_finisher board symbol
		compare_qualities (Just a) (Just b)
			| a_quality > b_quality = GT
			| b_quality > a_quality = LT
			| otherwise = EQ
			where
				a_quality = board_quality a symbol
				b_quality = board_quality b symbol

cpu_turn :: Board -> IO ()
cpu_turn board = do
	-- Output the board state.
	putStrLn (show board)

	-- Perform move.
	putStrLn "CPU turn..."
	let move = best_move board O
	putStrLn . show $ move

player_turn :: Board -> IO ()
player_turn board = do
	-- Output the board state.
	putStrLn (show board)

	-- Get user input.
	putStr "Enter your move: "
	input_string <- getLine
	let move = parse_move input_string

	-- Perform move.
	let new_board = make_move board move X

	case new_board of
		Just a_board ->
			cpu_turn a_board
		Nothing -> do
			putStrLn "Position already filled."
			player_turn board

main :: IO ()
main = player_turn blank_board