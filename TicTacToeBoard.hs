module TicTacToeBoard (
	Board (Board),
	Row (Row),
	Symbol (X, O, Blank),
	blank_board,
	get_symbol,
	set_symbol,
	flatten_rows,
	test_board_1,
	test_board_2
) where

data Symbol = X | O | Blank deriving Eq
data Row = Row Symbol Symbol Symbol
data Board = Board Row Row Row

instance Show Symbol where
	show s = case s of
		X -> "X"
		O -> "O"
		Blank -> "."
instance Show Row where
	show (Row a b c) = show a ++ " " ++ show b ++ " " ++ show c
instance Show Board where
	show (Board a b c) = "  0 1 2\n0 " ++ show a ++ "\n1 " ++ show b ++ "\n2 " ++ show c

blank_board :: Board
blank_board = Board (Row Blank Blank Blank) (Row Blank Blank Blank) (Row Blank Blank Blank)

get_symbol :: Board -> (Integer, Integer) -> Symbol
get_symbol (Board a b c) (x, y) = case y of
	0 -> get_row_symbol a
	1 -> get_row_symbol b
	2 -> get_row_symbol c
	_ -> error "Invalid input."
	where
		get_row_symbol (Row d e f) = case x of
			0 -> d
			1 -> e
			2 -> f
			_ -> error "Invalid input."

set_symbol :: Board -> (Integer, Integer) -> Symbol -> Board
set_symbol (Board a b c) (x, y) symbol = case y of
	0 -> Board (set_row_symbol a) b c
	1 -> Board a (set_row_symbol b) c
	2 -> Board a b (set_row_symbol c)
	_ -> error "Invalid input."
	where
		set_row_symbol (Row d e f) = case x of
			0 -> Row symbol e f
			1 -> Row d symbol f
			2 -> Row d e symbol
			_ -> error "Invalid input."

flatten_rows :: Board -> [Symbol]
flatten_rows (Board (Row a b c) (Row d e f) (Row g h i)) = [a, b, c, d, e, f, g, h, i]

test_board_1 :: Board
test_board_1 = Board (Row O O Blank) (Row X X O) (Row O X X)
test_board_2 :: Board
test_board_2 = Board (Row O O X) (Row X X O) (Row O Blank X)